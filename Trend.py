import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import pyodbc
import seaborn as sns


db = pyodbc.connect('Driver={SQL Server};'
                      'Server=LAPTOP-MKGF7OPL;'
                      'Database=FinalProject;'
                      'Trusted_Connection=yes;')

cursor = db.cursor()
sql='select left(convert(varchar, order_purchase_timestamp,23),7) as OrderDate, count(*) as count from olist_orders_dataset group by  left(convert(varchar, order_purchase_timestamp,23),7) order by OrderDate'
cursor.execute(sql)
result=cursor.fetchall()
db.commit()

x=[]
y=[]

for i in range(0,len(result)):
    x.append(result[i][0])
    y.append(result[i][1])

sns.relplot(x=x, y=y, kind="line", marker='o',)
plt.xticks(rotation=45)
plt.title("Monthly Order")
plt.xlabel("Month")
plt.ylabel("Order")
plt.show()